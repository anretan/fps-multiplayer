﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    public AudioClip shootSound;
    public GameObject lightObj;
    public ParticleSystem[] particles;
    public AudioSource aS;


    public void EnemyShootFireAnim()
    {
        StartCoroutine(Fire());
    }

    IEnumerator Fire()
    {
        lightObj.SetActive(true);
        foreach (ParticleSystem item in particles)
        {
            item.Emit(1);
        }
     
            aS.clip = shootSound;
            aS.PlayOneShot(shootSound);
        
        yield return new WaitForSeconds(0.1f);

        lightObj.SetActive(false);
    }
}
