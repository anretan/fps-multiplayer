﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
public class RoomListObject : MonoBehaviour
{
    [SerializeField]
    public Text roomName_Text;
    [SerializeField]
    private Text playerCounter_Text;

    public Button joinRoom_Bttn;

    public  RoomInfo RoomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roomInfo)
    {
        RoomInfo = roomInfo;
        playerCounter_Text.text = roomInfo.PlayerCount + "/" + roomInfo.MaxPlayers;
        roomName_Text.text = roomInfo.Name;
    }

}
