﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class ServersManager : MonoBehaviourPunCallbacks
{
    public static ServersManager instance;

    #region variables

    #endregion

    #region MB
    private void Awake()
    {
        instance = this;

        
     
    }

    #endregion

    #region ServerScripts

    public void ConnectToPhotonServer() 
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        MainMenuManager.instance.roomDebug_Text.text = "Connecting to server";
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.NickName = CoreManager.Instance.playerNick;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        MainMenuManager.instance.roomDebug_Text.text = "Connected to server";

        PhotonNetwork.JoinLobby();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        MainMenuManager.instance.roomDebug_Text.text = "Disconnected from server, reason: " + cause.ToString();
    }
    


 

    #endregion




}
