﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class RoomManager : MonoBehaviourPunCallbacks
{
    public static RoomManager instance;

    #region variables

    public RoomListObject roomListObjectPrefab;

    private List<RoomListObject> roomObjectsInMenu = new List<RoomListObject>();
    #endregion


    private void Awake()
    {
        instance = this;
    }

    #region functions



    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
       foreach(RoomInfo info in roomList)
        {
            if (info.RemovedFromList)
            {
                int index = roomObjectsInMenu.FindIndex(x => x.RoomInfo.Name == info.Name);
                if(index != -1)
                {
                    Destroy(roomObjectsInMenu[index].gameObject);
                    roomObjectsInMenu.RemoveAt(index);
                }
            }
            else
            {
                RoomListObject obj = Instantiate(roomListObjectPrefab, MainMenuManager.instance.roomListHandler.transform);
                if (obj != null)
                {
                    obj.SetRoomInfo(info);
                    obj.joinRoom_Bttn.onClick.AddListener(()=> { PhotonNetwork.JoinRoom(info.Name, null); });
                    roomObjectsInMenu.Add(obj);
                }
            }

                        
        }
    }


   

    public void CreateRoom(string roomName)
    {
        if (!PhotonNetwork.IsConnected)
        {
            MainMenuManager.instance.roomDebug_Text.text = "Can't connect to server, check internet connection!";
            return;
        }


        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 5;
        options.PlayerTtl = 5000;
        options.EmptyRoomTtl = 7000;

        PhotonNetwork.CreateRoom(roomName, options, TypedLobby.Default);
    }


    public override void OnCreatedRoom()
    {
        MainMenuManager.instance.roomDebug_Text.text = "Room created succesfully!";
    }

    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount >= 1)
        {
            PhotonNetwork.LoadLevel(1);
        }

    }

    //public override void OnJoinRoomFailed(short returnCode, string message)
    //{
    //    base.OnJoinRoomFailed(returnCode, message);
    //}

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        MainMenuManager.instance.roomDebug_Text.text = "Room creation failed: " + message.ToString();
    }


    public void StartGame()
    {
        if(PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            PhotonNetwork.LoadLevel(1);
        }
    }


    #endregion
}
