﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region variables
    public AudioSource audioSource;

    #endregion

    #region MB
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    #endregion

    #region soundFunctions
    public void PlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.PlayOneShot(audioClip);
    }

    public void StopPlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Stop();
    }

    #endregion
}
