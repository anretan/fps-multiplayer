﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance;

    #region variables

  

    public AudioManager audioManager;
   

    public string enemyPrefab;

    #endregion

    #region MB
    private void Awake()
    {
        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
       

        //PhotonNetwork.Instantiate(System.IO.Path.Combine("Enemy", enemyPrefab),
        //    MapSpawnManager.enemySpawnTransforms_List[Random.Range(0, MapSpawnManager.enemySpawnTransforms_List.Count - 1)].transform.position,
        //    Quaternion.identity
        //    );

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {


        base.OnPlayerEnteredRoom(newPlayer);
    }
    #endregion

}
