﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CoreManager : MonoBehaviour
{
    #region variables
    public static CoreManager Instance;

    public SaveManager saveManager;

    public string playerNick;

    #endregion

    #region MB

    private void Awake()
    {
        #region singleton dontdestroyonload
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        #endregion
    }

    void Start()
    {
        saveManager = FindObjectOfType<SaveManager>();
       
        StartCoroutine(LoadMenu());
    }





    #endregion


    #region core functions
    private IEnumerator LoadMenu()
    {
        saveManager.LoadAll();
        yield return new WaitForSeconds(0.2f);
        MainMenuManager.instance.StartMainMenu();
        
        ServersManager.instance.ConnectToPhotonServer();
    }

    #endregion
}
