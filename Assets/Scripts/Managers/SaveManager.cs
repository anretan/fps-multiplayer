﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    #region MainFunctions
    public void SaveAll()
    {
        SaveConst();
    }

    public void LoadAll()
    {
        LoadConst();
    }

    #endregion


    #region Save_LoadFunctions

    private void SaveConst()
    {
        PlayerPrefs.SetString("PlayerNick", CoreManager.Instance.playerNick);
    }

    private void LoadConst()
    {
        CoreManager.Instance.playerNick = PlayerPrefs.GetString("PlayerNick");
    }

    #endregion
}
