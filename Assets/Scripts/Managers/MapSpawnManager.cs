﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSpawnManager : MonoBehaviour
{
    #region variables
    [SerializeField]
    public GameObject enemySpawnPointsParent;
    public static List<Transform> enemySpawnTransforms_List = new List<Transform>();
    #endregion

    #region MB
    private void Awake()
    {
        SetSpawns();
    }
    #endregion

    #region functions
    private void SetSpawns()
    {
        foreach (Transform tran in enemySpawnPointsParent.GetComponentsInChildren<Transform>())
        {
            if (tran.name != enemySpawnPointsParent.name)
                enemySpawnTransforms_List.Add(tran);
        }
    }
    #endregion
}
