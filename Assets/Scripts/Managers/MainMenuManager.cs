﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager instance;

    #region variables
    public InputField nickName_InputField;
    public InputField roomName_InputField;
    public Button roomCreate_Bttn;
    public Text roomDebug_Text;

    public GameObject roomListHandler;
    CoreManager cm;
    #endregion

    #region MB

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        cm = CoreManager.Instance;
        nickName_InputField.onEndEdit.AddListener(delegate { OnNicknameChanged(nickName_InputField.text); });
        roomCreate_Bttn.onClick.AddListener(() => { CreateRoom(); });
    }

    #endregion


    #region menuElementsFunctions

    private void CreateRoom()
    {
        if(roomName_InputField.text != null)
            RoomManager.instance.CreateRoom(roomName_InputField.text);
        else
            RoomManager.instance.CreateRoom("Room" + Random.Range(0,99));
    }


    public void StartMainMenu()
    {
        nickName_InputField.text = cm.playerNick;
    }

    private void OnNicknameChanged(string nick)
    {
        cm.playerNick = nick;
        cm.saveManager.SaveAll();
    }
    #endregion
}
