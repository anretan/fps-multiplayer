﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponShooting : MonoBehaviour
{
    #region variables

    public GameObject bulletHolePrefab, aimDot;
    public LayerMask canBeShot;
    public Camera bulletSpawn;
    private float recoil;

    private Vector3 weaponZeroPosition;
    private float weaponShakePower, weaponKickbackPower;

    private PlayerManager playerManager;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        bulletSpawn = GameObject.Find("PlayerCam").GetComponent<Camera>();
        aimDot = GameObject.Find("AimDot");
    }

    // Update is called once per frame
    void Update()
    {
        if (playerManager.actualWeapon != null)
        {
            if (Input.GetMouseButtonDown(0))
                Shoot();

            SetRecoil(Input.GetMouseButton(1));
            SetWeaponAimPos(Input.GetMouseButton(1));

            //set weapon position back to normal;
            playerManager.actualWeapon.transform.localPosition =
                Vector3.Lerp(playerManager.actualWeapon.transform.localPosition, weaponZeroPosition, Time.deltaTime * 4f);
        }
    }


    private void SetRecoil(bool isScoping)
    {
        recoil = playerManager.WeaponPrefabsList[PlayerManager.currentWeaponId].GetComponent<WeaponObject>().recoil;
        if (isScoping)
        {
            recoil = playerManager.WeaponPrefabsList[PlayerManager.currentWeaponId].GetComponent<WeaponObject>().recoil / 4;
        }
    }

    private void SetWeaponAimPos(bool isScoping)
    {
        WeaponObject weaponObject = playerManager.WeaponPrefabsList[PlayerManager.currentWeaponId].GetComponent<WeaponObject>();

        if (isScoping)
        {
            playerManager.actualWeapon.transform.localPosition =
                Vector3.Lerp(playerManager.actualWeapon.transform.localPosition,
                weaponObject.scopeAimPos, 
                Time.deltaTime * 5);

            playerManager.actualWeapon.transform.localRotation =
                Quaternion.Lerp(playerManager.actualWeapon.transform.localRotation,
               Quaternion.Euler(weaponObject.scopeRotation),
                Time.deltaTime * 5);
            aimDot.GetComponent<Image>().color = new Color(255, 255, 255, 0);

            weaponKickbackPower = weaponObject.kickback / 5;
            weaponShakePower = weaponObject.weaponShakeRotation / 5;

            weaponZeroPosition = weaponObject.scopeAimPos;
        }
        else
        {
            playerManager.actualWeapon.transform.localPosition =
               Vector3.Lerp(playerManager.actualWeapon.transform.localPosition,
               weaponObject.armAimPos,
               Time.deltaTime * 5);

            playerManager.actualWeapon.transform.localRotation =
              Quaternion.Lerp(playerManager.actualWeapon.transform.localRotation,
             Quaternion.Euler(weaponObject.armRotation),
              Time.deltaTime * 5);

            aimDot.GetComponent<Image>().color = new Color(255, 255, 255, 255);

            weaponKickbackPower = weaponObject.kickback;
            weaponShakePower = weaponObject.weaponShakeRotation;

            weaponZeroPosition = weaponObject.armAimPos;
        }
    }


    private void Shoot()
    {
        WeaponObject weaponObject = playerManager.actualWeapon.GetComponent<WeaponObject>();
        // recoil
        Vector3 weaponRecoil = aimDot.transform.position;
        weaponRecoil = new Vector2(weaponRecoil.x + (Random.Range(-recoil, recoil) / 10), weaponRecoil.y + (Random.Range(-recoil, recoil) / 10));

        //instantiate bulletHoles after shoot
        Ray ray = bulletSpawn.ScreenPointToRay(weaponRecoil);

        if (Physics.Raycast(ray, out RaycastHit aimHit, 1000f, canBeShot))
        {
            GameObject newHole = Instantiate(bulletHolePrefab, aimHit.point + aimHit.normal * 0.001f, Quaternion.identity) as GameObject;
            newHole.transform.LookAt(aimHit.point + aimHit.normal);
            Destroy(newHole, 3f);
        }


        //weapon FX
        //sound
        GameManager.instance.audioManager.PlaySound(weaponObject.shootSound);

        //kickbak
        playerManager.actualWeapon.transform.position += playerManager.actualWeapon.transform.forward * weaponKickbackPower;
        playerManager.actualWeapon.transform.Rotate(-weaponShakePower, 0, 0);

        //mullFx
        weaponObject.ShootFireAnim();
    }
}
