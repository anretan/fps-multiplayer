﻿using System.Collections;
using UnityEngine;

public class WeaponObject : MonoBehaviour
{
    public string weaponName;
    public float firerate;
    public float recoil;
    public float weaponShakeRotation;
    public float kickback;
    public Transform handParentObj;
    public Vector3 armRotation;
    public Vector3 scopeRotation;
    public Vector3 armAimPos;
    public Vector3 scopeAimPos;
    public AudioClip shootSound;
    public GameObject lightObj;
    public ParticleSystem[] particles;

    public void ShootFireAnim()
    {
        StartCoroutine(Fire());
    }

    IEnumerator Fire()
    {
        lightObj.SetActive(true);
        foreach (ParticleSystem item in particles)
        {
            item.Emit(1);
        }

        yield return new WaitForSeconds(0.1f);

        lightObj.SetActive(false);
    }

}
