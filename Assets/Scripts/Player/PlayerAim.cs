﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviourPunCallbacks
{

    #region variables

    public Transform playerBodyXaim;
    public Transform playerTorsoYaim;    //rotating body to get Yaxes , rotate torso with weapon and camera to get X axes
    public Transform weaponSpawnTransform;

    public bool cursorLock = true;

    public float xSens;
    public float ySens;
    public float maxAngle;

    private Quaternion yCenter;

    #endregion

    #region MonoBehaviour Callbacks   
    void FixedUpdate()
    {
        if (!photonView.IsMine) return;

        SetY();
        SetX();
        UpdateCursorLock();
    }
    #endregion

    #region moveXY
    private void SetY()
    {
        float playerInput = Input.GetAxis("Mouse Y") * ySens * Time.deltaTime;
        Quaternion playerAdj = Quaternion.AngleAxis(playerInput, -Vector3.right);
        Quaternion delta = playerTorsoYaim.localRotation * playerAdj;

        if(Quaternion.Angle(yCenter, delta) < maxAngle)
            playerTorsoYaim.localRotation = delta;
    }


    private void SetX()
    {
        float playerInput = Input.GetAxis("Mouse X") * xSens * Time.deltaTime;
        Quaternion playerAdj = Quaternion.AngleAxis(playerInput, Vector3.up);
        Quaternion delta = playerBodyXaim.localRotation * playerAdj;

        playerBodyXaim.localRotation = delta;
    }
    #endregion


    public void SetPlayerAim()
    {
        yCenter = playerTorsoYaim.localRotation;
    }

    private void UpdateCursorLock()
    {
        if (cursorLock)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            if (Input.GetKeyDown(KeyCode.Escape))
                cursorLock = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
         
            if (Input.GetKeyDown(KeyCode.Escape))
                cursorLock = true;

        }
    }

}
