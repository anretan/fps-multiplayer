﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerMovement : MonoBehaviourPunCallbacks

{
    #region variables
    public float playerSpeed;

    public float jumpForce;

    public Transform groundDetector;
    public LayerMask ground;

    private Rigidbody playerRB;
    public bool isGrounded;
    private bool isJumping;
    private bool jump;
    private float hMove;
    private float vMove;
    #endregion

    #region MB
    void Start()
    {
        if (!photonView.IsMine) return;

        playerRB = GetComponent<Rigidbody>();      
    }

    private void Update()
    {
        if (!photonView.IsMine) return;

        hMove = Input.GetAxisRaw("Horizontal");
        vMove = Input.GetAxisRaw("Vertical");
        isGrounded = Physics.Raycast(groundDetector.position, Vector3.down, 0.1f, ground);
        jump = Input.GetKeyDown(KeyCode.Space);

        //Jumping
        if (jump && isGrounded)
            playerRB.AddForce(Vector3.up * jumpForce * 100);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!photonView.IsMine) return;
        //moving
        Vector3 playerDirection = new Vector3(hMove, 0, vMove);
        playerDirection.Normalize();

        Vector3 playerVelocity = transform.TransformDirection(playerDirection) * playerSpeed * Time.deltaTime;
        playerVelocity.y = playerRB.velocity.y;
        playerRB.velocity = playerVelocity;
    }

    #endregion
}
