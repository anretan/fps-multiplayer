﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerManager : MonoBehaviourPunCallbacks
{
    public static PlayerManager instance;

    #region variables
    public List<GameObject> WeaponPrefabsList = new List<GameObject>();
    public GameObject actualWeapon;
    public GameObject playerHands;
    public static int currentWeaponId;
    public PlayerAim playerAim;
  
    #endregion

    #region MB

    private void Awake()
    {
        instance = this;
       
    }
    void Start()
    {
       

        playerAim = FindObjectOfType<PlayerAim>();

        if (photonView.IsMine)
            playerAim.SetPlayerAim();
        // GameManager.instance.playerAim.SetPlayerAim();  //narazie, po wprowadzeniu zmiany skinow postaci, wywolac po kazdym skinie
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine) return;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (actualWeapon != null)
                Destroy(actualWeapon);

            currentWeaponId = 0;
            WeaponObject wo = WeaponPrefabsList[0].GetComponent<WeaponObject>();
            GameObject weapon = Instantiate(WeaponPrefabsList[0], new Vector3(0, 0, 0), Quaternion.identity, parent: playerAim.weaponSpawnTransform);

            playerHands.transform.parent = weapon.transform;
            playerHands.transform.localPosition = wo.handParentObj.transform.localPosition;
            playerHands.transform.localRotation = wo.handParentObj.transform.localRotation;
            playerHands.SetActive(true);
            weapon.transform.localPosition = wo.armAimPos;
            weapon.transform.localRotation = Quaternion.Euler(wo.armRotation.x, wo.armRotation.y, wo.armRotation.z);
            actualWeapon = weapon;


        }

    }

    #endregion
}
