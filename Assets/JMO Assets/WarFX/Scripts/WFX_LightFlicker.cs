using UnityEngine;
using System.Collections;

/**
 *	Rapidly sets a light on/off.
 *	
 *	(c) 2015, Jean Moreno
**/

[RequireComponent(typeof(Light))]
public class WFX_LightFlicker : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<Light>().enabled = true;
    }

    private void OnDisable()
    {
        GetComponent<Light>().enabled = false;
    }
}
